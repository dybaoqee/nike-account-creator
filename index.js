var express = require('express');
var bodyParser = require('body-parser');
var session = require('express-session');
var cookies = require('cookie-parser');
var flash = require('express-flash');
var fs = require('fs');
var path = require('path');

var DEF = require('./constants');
var SaveController = require('./src/controller/SaveController');
var MessageController = require('./src/controller/MessageController');
require('./src/core/db');
var Settings = require('./src/models/all').Setting;
var app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.set('port', (process.env.PORT || 5000));
app.use(cookies());
app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: "sadh98dnqxd88a!((JD0md02du3mmqdjm@oDJOIQOWIJEQIOWE"
}));
app.use(flash());
app.use('/public', express.static(path.join(__dirname , 'public')));
app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'src/views'));

app.get('/', (req, res) => {
    var file = 'index';
    if (!(req.session && req.session.user)) {
        res.render(file);
        return;
    }
    file = 'info';
    Settings.find({}, (err, docs) => {
        var gmails = fs.readFileSync(path.join('files', 'emails.txt')).toString();
        var proxies = fs.readFileSync(path.join('files', 'proxies.txt')).toString();
        res.render(file, { status: {}, gmails: gmails, proxies: proxies, setting: docs });
    })
});

app.post('/',  (req, res) => {
    if (req.body.username && req.body.password) {
        if (req.body.username === 'hariman' && req.body.password === 'pass') {
            req.session.user = req.body.username;
            req.flash('status', { message: 'Welcome!', css: 'success' });
        } else {
            req.flash('status', { message: 'Login failed!', css: 'failed' });
        }
        res.redirect('/');
    }
});

app.all('/sms', MessageController.saveIncoming);

app.get('/start', function (req, res) {
    if (!req.session.user) {
        return res.redirect('/');
    }
});

app.get('/archive', MessageController.getArchived);
app.get('/empty', MessageController.emptify);
app.get('/inbox', MessageController.getInbox);

app.post('/save/proxy', SaveController.saveProxy);
app.post('/save/setting', SaveController.saveSetting);
app.post('/save/personal', SaveController.savePersonal);
app.post('/save/plivo', SaveController.savePlivo);
app.post('/save/emails', SaveController.saveEmails);

app.listen(app.get('port'), function() {
    console.log('Node app is running on port', app.get('port'));
});