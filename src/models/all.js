module.exports = {
  Number: require('./Number'),
  Gmail: require('./Gmail'),
  Setting: require('./Setting'),
  Message: require('./Message')
}
