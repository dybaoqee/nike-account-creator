var Model = require('../models/all');

function saveIncoming(req, res) {
  var from_number = request.body.From || request.query.From;
  var to_number = request.body.To || request.query.To;
  var text = request.body.Text || request.query.Text;
  response.send('Message received from: ' + from_number + ': ' + text);
  Model.Message.create({
    sender: from_number,
    content: text,
    receiver: to_number
  }, (err, res) => {
    console.log(err || res);
  });
}

function getArchived(req, res) {

}

function emptify(req, res) {

}

function getInbox(req, res) {
  
}

module.exports = {
  saveIncoming: saveIncoming,
  getArchived: getArchived,
  emptify: emptify,
  getInbox: getInbox
}