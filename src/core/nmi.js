var nightmare = require('nightmare')(
    {
        show: true,
        waitTimeout: 30000
    }
);

module.exports = nightmare;