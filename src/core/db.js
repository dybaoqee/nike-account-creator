var mongoose = require('mongoose');

var configs = {
  url: 'mongodb://localhost/nikesnkrs',
  options: {
    db: {
      safe: true
    },
    server: {
      socketOptions: {
        keepAlive: 1
      }
    }
  }
}

mongoose.Promise = require('bluebird');

module.exports = mongoose.connect(configs.url, configs.options);