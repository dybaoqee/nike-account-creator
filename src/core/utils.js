var fs = require('fs');
var request = require('request');

//Sources of messages
const Source = {
    NIKE: 0,
    GOOGLE: 1
}
const URL = '';
const NAMES = [
    "Kanye West",
    "Lorne Michaels",
    "Mellody Hobson",
    "Tim Cook",
    "Elizabeth Holmes",
    "Charles Koch",
    "David Koch",
    "Susan Wojcicki",
    "Chanda Kochhar",
    "Tony Fernandes",
    "Lee Daniels",
    "Reid Hoffman",
    "Kim Kardashian",
    "Janet Yellen",
    "Danny Meyer",
    "Lei Jun",
    "Bob Iger",
    "Satya Nadella",
    "Jorge Paulo Lemann",
    "Misty Copeland",
    "Scott Kelly",
    "Emmanuelle Charpentier",
    "Jennifer Doudna",
    "Brian Chesky",
    "Jimmy Lai",
    "Emma Watson",
    "Vikram Patel",
    "Pardis Sabeti",
    "Reese Witherspoon",
    "Bryan Stevenson",
    "Chai Jing",
    "Magnus MacFarlane-Barrow",
    "Kira Orange Jones",
    "Aura Elena Farfán",
    "Martin Blaser",
    "Anita Sarkeesian",
    "Tom Catena",
    "Rudolph Tanzi",
    "Mustafa Hassan",
    "Laverne Cox",
    "Sarah Koenig",
    "Bradley Cooper",
    "Richard Linklater",
    "Chris Ofili",
    "Julianna Margulies",
    "Amy Schumer",
    "Alexander Wang",
    "Jill Soloway",
    "Chris Pratt",
    "Audra McDonald",
    "Tim McGraw",
    "Kevin Hart",
    "Chimamanda Adichie",
    "Julianne Moore",
    "Christopher Nolan",
    "Marie Kondo",
    "John Oliver",
    "Jorge Ramos",
    "Narendra Modi",
    "Angela Merkel",
    "Bob Corker",
    "Rula Ghani",
    "Muhammadu Buhari",
    "Alexis Tsipras",
    "Vladimir Putin",
    "Obiageli Ezekwesili",
    "Elizabeth Warren",
    "Haider Al-Abadi",
    "Joko Widodo",
    "Salman Abdulaziz",
    "Xi Jinping",
    "Jeb Bush",
    "Tom Frieden",
    "Samantha Power",
    "Raúl Castro",
    "Kim Jong Un",
    "Abubakar Shekau",
    "Benjamin Netanyahu",
    "Hillary Clinton",
    "Martin Dempsey",
    "Beji Essebsi",
    "Adam Silver",
    "Lu Wei",
    "Marine Le Pen",
    "Barack Obama",
    "Mitch McConnell",
    "Mohammad Zarif",
    "Joanne Liu",
    "Ruth Ginsburg",
    "Taylor Swift",
    "Diane Furstenberg",
    "Gabriel Medina",
    "Haruki Murakami",
    "Jerry Brown",
    "Abby Wambach",
    "Ina Garten",
    "Thomas Piketty",
    "Malala Yousafzai",
    "Pope Francis"
];

function getFirstName() {
    return NAMES[Math.floor(Math.random() * NAMES.length)].split(' ')[0];
}

function getLastName() {
    return NAMES[Math.floor(Math.random() * NAMES.length)].split(' ')[1];
}

function getEmail() {
    return getFromFile('emails.txt');
}

function getAuthEmail() {
    return getFromFile('emails-auth.txt');
}

function getFromFile(filename) {
    var emails = fs.readFileSync(filename).toString().split('\n');
    if (emails.length > 0) {
        var email = emails.pop().trim();
        fs.writeFileSync(filename, emails.join('\n'));
        return email;
    }
    return '';
}

function getBirthDate() {
    var dd = Math.floor(Math.random() * 29) + "";
    if (dd.length === 1) dd = '0' + dd;
    var mm = Math.floor(Math.random() * 12) + "";
    if (mm.length === 1) mm = '0' + mm;
    if (dd === '00') dd = '01';
    if (mm === '00') mm = '01';
    var yyyy = (new Date().getFullYear()) - (parseInt(dd) > 15 ? parseInt(dd) : 15);
    return dd + '/' + mm + '/' + yyyy;
}

function getNextNumber() {
    return getFromFile('numbers.txt');
}

function getPlivoNumber() {
    return getFromFile('plivo-numbers.txt');
}

var inbox;
function pullVerificationCode(source, receiver) {
    inbox = null;
    var code = '';
    readInbox(receiver);
    while (!inbox) {
        //do nothing wait for reading inbox
    }
    if (source == Source.NIKE) {
        code = inbox; //TODO find inbox
    } else if (source == Source.GOOGLE) {
        code = inbox; //TODO detect code
    }
    return code;
}

function readInbox(receiver) {
    request.get(URL + '/inbox', function (error, response, data) {
        if (data) {
            for (var i = 0; i < data.length; i++) {
                if (data[i].to == receiver) {
                    inbox = data[i].message;
                    request.get(URL + '/empty', function (e, r, d) { });
                    break;
                }
            }
        }
        console.log(typeof data);
        wait = false;
    });
}

function exportNumber(email, password, number) {
    var three = number + '\n' + number + '\n' + number + '\n';
    fs.appendFile('gvoice.csv', email + ',' + password + ',' + number + '\n', function (err) {
        if (err) {
            exportNumber(email, password, number);
        } else {
            fs.appendFile('numbers.txt', three, function (er) {
                if (er) {
                    exportNumber(email, password, number);
                }
            });
        }
    })
}

function randomString(length) {
    var str = [];
    var nums = '0123456789'.split('');
    var alpha = 'abcdefghijklmnopqrstuvwyz'.split('');
    str.push(alpha[Math.floor(Math.random() * alpha.length)]);
    alpha = alpha.concat(nums);
    for(var i = parseInt(length) - 1; i > 0; i--) {
        str.push(alpha[Math.floor(Math.random() * alpha.length)]);
    }
    return str.join('');
}

function generatePassword(email) {
    var chars = [];
    for(var i in email) {
        chars.push(String.fromCharCode(email.charCodeAt(i) + 5));
    }
    return chars.join('');
}

function saveNikeAccount(email, password, number) {
    fs.appendFile('nike.csv', email + ',' + password + ',' + number + '\n', (err) => {
        if (err) {
            saveNikeAccount(email, password, number);
        }
    })
}

function getPlivoNumberForGmail() {
    return '2480247123'
}

function getAnyEmail() {
    return 'nodehari@gmail.com';
}

module.exports = {
    getBirthDate: getBirthDate,
    getFirstName: getFirstName,
    getLastName: getLastName,
    getNextNumber: getNextNumber,
    getEmail: getEmail,
    getAuthEmail: getAuthEmail,
    pullVerificationCode: pullVerificationCode,
    Source: Source,
    saveNikeAccount: saveNikeAccount,
    exportNumber: exportNumber,
    getPlivoNumber: getPlivoNumber,
    randomString: randomString,
    generatePassword: generatePassword,
    getPlivoNumberForGmail: getPlivoNumberForGmail,
    getAnyEmail: getAnyEmail
}