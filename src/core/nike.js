

var Nightmare = require('nightmare');

var utils = require('./utils');

const PASSWORD = "PassWord#$%123";


function createNext(callback) {

  var creator = Nightmare({
    show: true,
    waitTimeout: 120000
  });

  var number = utils.getNextNumber();
  if (number == "") {
    console.log('Number seems to be empty');
    return;
  }
  var email = utils.getEmail();
  if (email == "") {
    console.log('Email seems to be empty');
    return;
  }
  creator
    .goto('https://www.nike.com/launch/?s=upcoming')
    .click('.js-log-in')
    .wait('input[name="emailAddress"]')
    .click('input[value="JOIN NOW"]')
    .type('input[name="emailAddress"]', email)
    .type('input[name="password"]', PASSWORD)
    .type('input[name="firstName"]', utils.getFirstName())
    .type('input[name="lastName"]', utils.getLastName())
    .click('ul[data-componentname="gender"] > li:first-child')
    .type('input[name="dateOfBirth"]', utils.getBirthDate())
    .click('input[value="CREATE ACCOUNT"]')
    .wait('input[name="verifyMobileNumber"]')
    .type('input[name="verifyMobileNumber"]', number.trim().replace(/[^\d]+/, ''))
    .click('input[value="SEND CODE"]')
    .wait('input[data-componentname="code"]')
    .then(() => {
      console.log('Done sending SMS.');
      var code = '231213';//utils.pullVerificationCode(utils.Source.NIKE))
      var codePuller = setInterval(function () {
        creator
          .type('input[data-componentname="code"]', code)
          .click('input[value="SUBMIT"]')
          .then(function () {
            console.log('Verified');
            utils.saveNikeAccount(email, PASSWORD, number);
          });
      }, 5000);
    })
    .catch(err => {
      if (err) {
        callback(err, null)
      }
    })
}
